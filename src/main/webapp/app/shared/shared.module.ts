import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { LaneSharedLibsModule, LaneSharedCommonModule, HasAnyAuthorityDirective } from './';

@NgModule({
    imports: [LaneSharedLibsModule, LaneSharedCommonModule],
    declarations: [HasAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    exports: [LaneSharedCommonModule, HasAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LaneSharedModule {
    static forRoot() {
        return {
            ngModule: LaneSharedModule
        };
    }
}
