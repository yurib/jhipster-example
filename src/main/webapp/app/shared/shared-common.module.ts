import { NgModule } from '@angular/core';

import { LaneSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [LaneSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [LaneSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class LaneSharedCommonModule {}
